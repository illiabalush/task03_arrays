package task_b;

import helper.ArrayHelper;

public class B {
    public static void main(String[] args) {
        int[] array;
        ArrayHelper.logger.info("Enter size of array: ");
        array = new int[ArrayHelper.scanner.nextInt()];
        ArrayHelper.fillArray(array);
        ArrayHelper.showArray(array);
        int[] uniqueElementsArray = new int[array.length];
        int countUniqueElements = 0;
        for (int i = 0; i < array.length; i++) {
            if (!ArrayHelper.checkMultiplyValue(array, array[i])) {
                uniqueElementsArray[countUniqueElements] = array[i];
                countUniqueElements++;
            }
        }
        array = null;
        uniqueElementsArray = ArrayHelper.trimToRealSize(uniqueElementsArray, countUniqueElements);
        ArrayHelper.logger.info("Unique elements: ");
        ArrayHelper.showArray(uniqueElementsArray);
    }
}
