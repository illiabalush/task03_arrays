package helper;

import java.util.Scanner;

import org.apache.logging.log4j.*;

public class ArrayHelper {
    public static Logger logger = LogManager.getLogger(ArrayHelper.class);
    public static Scanner scanner = new Scanner(System.in);

    private ArrayHelper() {
    }

    public static void fillArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            logger.info("Enter [" + i + "] : ");
            array[i] = scanner.nextInt();
        }
    }

    public static void showArray(int[] array) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            stringBuilder.append(array[i]).append(" ");
        }
        logger.info(stringBuilder.toString());
    }

    public static boolean contains(int[] array, int size, int element) {
        for (int i = 0; i < size; i++) {
            if (array[i] == element) {
                return true;
            }
        }
        return false;
    }

    public static int[] trimToRealSize(int[] array, int size) {
        int[] trimArray = new int[size];
        for (int i = 0; i < size; i++) {
            trimArray[i] = array[i];
        }
        return trimArray;
    }

    public static boolean checkMultiplyValue(int[] array, int element) {
        int countCoincidence = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == element) {
                countCoincidence++;
            }
            if (countCoincidence == 2) {
                return true;
            }
        }
        return false;
    }
}
