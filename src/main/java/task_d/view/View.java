package task_d.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task_d.model.objects.GameObject;

import java.util.Arrays;

public class View {

    public static Logger logger = LogManager.getLogger(View.class);

    public void selectName() {
        logger.info("Enter your name: ");
    }

    public void showFunctions() {
        logger.info("1 - show all doors");
        logger.info("2 - show number of death doors");
        logger.info("3 - show right way");
        logger.info("0 - exit");
    }

    public void showDoors(GameObject[] gameObjects) {
        StringBuilder stringBuilderIndex = new StringBuilder();
        StringBuilder stringBuilderObjectNames = new StringBuilder();
        StringBuilder stringBuilderObjectPower = new StringBuilder();
        for (int i = 0; i < gameObjects.length; i++) {
            String className = gameObjects[i].getClass()
                    .getName().substring((gameObjects[i].getClass()
                            .getName().lastIndexOf('.') + 1));

            stringBuilderIndex.append("\t\t").append(i).append("\t   ")
                    .append(getSpaces(className.length()));

            stringBuilderObjectNames.append("\t").append(className)
                    .append("\t\t");

            stringBuilderObjectPower.append("\t\t")
                    .append(gameObjects[i].getPower()).append("\t")
                    .append(getSpaces(className.length()));
        }
        logger.info("\n" + stringBuilderIndex + "\n" +
                stringBuilderObjectNames + "\n" + stringBuilderObjectPower);
    }

    private String getSpaces(int numberOfSpaces) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < numberOfSpaces; i++) {
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }

    public void showDeathDoors(int numberDeadlyDoors) {
        logger.info(numberDeadlyDoors + " doors are deadly");
    }

    public void showRightWay(int[] rightWay) {
        logger.info("The right way is: " + Arrays.toString(rightWay));
    }

    public void showIncorrectInput() {
        logger.info("Try again");
    }
}
