package task_d.controller;

import task_d.model.game.Game;
import task_d.view.View;

import java.util.Scanner;

import static task_d.model.constants.Constant.MAX_NUMBER_DOORS;

public class Controller {
    private View view;
    private Scanner scanner = new Scanner(System.in);
    private Game game;

    public Controller() {
        view = new View();
        game = new Game();
    }

    public void start() {
        view.selectName();
        game.getPlayer().setPlayerName(scanner.nextLine());
        view.showFunctions();
        int option = 0;
        while (true) {
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    view.showDoors(game.getDoors());
                    break;
                case 2:
                    view.showDeathDoors(game.getNumberDeathDoors
                            (game.getDoors(), 0, 0));
                    break;
                case 3:
                    view.showRightWay(game.getRightWay());
                    break;
                case 0:
                    return;
                default:
                    view.showIncorrectInput();
            }
        }
    }
}
