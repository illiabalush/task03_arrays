package task_d.application;

import task_d.controller.Controller;

public class Application {
    public static void main(String[] args) {
        new Controller().start();
    }
}
