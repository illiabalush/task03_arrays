package task_d.model.game;

import task_d.model.objects.Artifact;
import task_d.model.objects.GameObject;
import task_d.model.objects.Hero;
import task_d.model.objects.Player;

import static task_d.model.constants.Constant.MAX_NUMBER_DOORS;

public class Game {
    private Player player;
    private GameObject[] doors = new GameObject[MAX_NUMBER_DOORS];

    public Game() {
        player = new Player();
        generateOutdoorsObjects();
    }

    private void generateOutdoorsObjects() {
        for (int i = 0; i < doors.length; i++) {
            doors[i] = getRandomObject();
        }
    }

    private GameObject getRandomObject() {
        return ((Math.random() * 10) >= 5) ? new Hero() : new Artifact();
    }

    public int[] getRightWay() {
        int[] rightWayDoors = new int[MAX_NUMBER_DOORS];

        if ((player.getPower() + getGeneralFriendlyPower()) < getGeneralEnemyPower()) {
            return new int[0];
        }
        for (int i = 0, k = 0, j = doors.length - 1; i < doors.length; i++) {
            if (doors[i].isEnemy()) {
                rightWayDoors[j] = i;
                j--;
            } else {
                rightWayDoors[k] = i;
                k++;
            }
        }
        return rightWayDoors;
    }

    private int getGeneralFriendlyPower() {
        int generalFriendlyPower = 0;
        for (int i = 0; i < doors.length; i++) {
            if (!doors[i].isEnemy()) {
                generalFriendlyPower += doors[i].getPower();
            }
        }
        return generalFriendlyPower;
    }

    private int getGeneralEnemyPower() {
        int generalEnemyPower = 0;
        for (int i = 0; i < doors.length; i++) {
            if (doors[i].isEnemy()) {
                generalEnemyPower += doors[i].getPower();
            }
        }
        return generalEnemyPower;
    }

    public int getNumberDeathDoors(GameObject[] doors, int indexDoor, int countDeathDoors) {
        if (indexDoor == doors.length - 1) return countDeathDoors;
        if ((doors[indexDoor].isEnemy()) &&
                (doors[indexDoor].getPower() > player.getPower())) {
            ++countDeathDoors;
        }
        return getNumberDeathDoors(doors, ++indexDoor, countDeathDoors);
    }

    public GameObject getDoor(int door) {
        return doors[door];
    }

    public Player getPlayer() {
        return player;
    }

    public GameObject[] getDoors() {
        return doors;
    }
}
