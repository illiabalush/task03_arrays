package task_d.model.constants;

public class Constant {
    private Constant() {
    }

    public static final int INITIAL_POWER = 25;
    public static final int MAX_NUMBER_DOORS = 10;
    public static final int MIN_ARTIFACT_POWER = 10;
    public static final int MAX_ARTIFACT_POWER = 80;
    public static final int MIN_ENEMY_POWER = 5;
    public static final int MAX_ENEMY_POWER = 100;
}
