package task_d.model.objects;

import static task_d.model.constants.Constant.*;

public class Hero extends GameObject {
    public Hero() {
        isEnemy = true;
        power = getRandomPower();
    }

    @Override
    protected int getRandomPower() {
        return (int)(Math.random() * (MAX_ENEMY_POWER - MIN_ENEMY_POWER)) + MIN_ENEMY_POWER;
    }
}
