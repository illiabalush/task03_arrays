package task_d.model.objects;

public abstract class GameObject {
    protected int power;
    protected boolean isEnemy;

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public boolean isEnemy() {
        return isEnemy;
    }

    public void setEnemy(boolean enemy) {
        isEnemy = enemy;
    }

    protected abstract int getRandomPower();

    @Override
    public String toString() {
        return getClass().getName() + " " + isEnemy() + " " + getPower();
    }
}
