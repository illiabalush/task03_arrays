package task_d.model.objects;

import static task_d.model.constants.Constant.INITIAL_POWER;

public class Player {
    private int power;
    private String playerName;

    public Player() {
        power = INITIAL_POWER;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    @Override
    public String toString() {
        return getClass().getName() + " " + power;
    }
}
