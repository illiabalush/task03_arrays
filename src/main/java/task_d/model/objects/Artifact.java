package task_d.model.objects;

import static task_d.model.constants.Constant.*;

public class Artifact extends GameObject {
    public Artifact() {
        isEnemy = false;
        power = getRandomPower();
    }

    @Override
    protected int getRandomPower() {
        return (int)(Math.random() * (MAX_ARTIFACT_POWER - MIN_ARTIFACT_POWER)) + MIN_ARTIFACT_POWER;
    }
}
