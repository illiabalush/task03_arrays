package task_a;

import helper.ArrayHelper;

public class A {
    public static void main(String[] args) {
        int[] firstArray;
        int[] secondArray;
        ArrayHelper.logger.info("Enter size of first array: ");
        firstArray = new int[ArrayHelper.scanner.nextInt()];
        ArrayHelper.logger.info("Enter size of second array: ");
        secondArray = new int[ArrayHelper.scanner.nextInt()];
        ArrayHelper.fillArray(firstArray);
        ArrayHelper.fillArray(secondArray);
        ArrayHelper.showArray(firstArray);
        ArrayHelper.showArray(secondArray);
        int[] commonElementsArray = new int[firstArray.length + secondArray.length];
        int[] uniqueElementsArray = new int[firstArray.length + secondArray.length];
        int countCommonElements = 0;
        int countUniqueElements = 0;
        for (int i = 0; i < firstArray.length; i++) {
            if (ArrayHelper.contains(secondArray, secondArray.length, firstArray[i])) {
                commonElementsArray[countCommonElements] = firstArray[i];
                countCommonElements++;
            } else {
                uniqueElementsArray[countUniqueElements] = firstArray[i];
                countUniqueElements++;
            }
        }
        for (int i = 0; i < secondArray.length; i++) {
            if (!ArrayHelper.contains(firstArray, firstArray.length, secondArray[i])) {
                uniqueElementsArray[countUniqueElements] = secondArray[i];
                countUniqueElements++;
            }
        }
        commonElementsArray = ArrayHelper.trimToRealSize(commonElementsArray, countCommonElements);
        uniqueElementsArray = ArrayHelper.trimToRealSize(uniqueElementsArray, countUniqueElements);
        ArrayHelper.logger.info("Array with common elements: ");
        ArrayHelper.showArray(commonElementsArray);
        ArrayHelper.logger.info("Array with different elements: ");
        ArrayHelper.showArray(uniqueElementsArray);
    }
}
