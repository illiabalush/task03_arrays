package task_c;

import helper.ArrayHelper;

public class C {
    public static void main(String[] args) {
        int[] array;
        ArrayHelper.logger.info("Enter size of array: ");
        array = new int[ArrayHelper.scanner.nextInt()];
        ArrayHelper.fillArray(array);
        ArrayHelper.showArray(array);
        int[] uniqueElementsArray = new int[array.length];
        int countUniqueElements = 0;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] == array[i + 1] &&
                    !ArrayHelper.contains(uniqueElementsArray, countUniqueElements, array[i])) {
                uniqueElementsArray[countUniqueElements] = array[i];
                countUniqueElements++;
            }
        }
        uniqueElementsArray = ArrayHelper.trimToRealSize(uniqueElementsArray, countUniqueElements);
        ArrayHelper.logger.info("Unique elements: ");
        ArrayHelper.showArray(uniqueElementsArray);
    }
}